#include <vector>
#include <random>
#include <cmath>

int main()
{
    std::random_device rd{};
    std::mt19937 gen{ rd() };

    //to histograms for adding
    Histogram histadd1(10,42,25);
    Histogram histadd2(10, 28, 7);
    std::normal_distribution<> d{ 5,2 };

    for (int i = 0; i < 1000; ++i) {
        histadd1.fill(d(gen));
    }

    //assigning operator
    histadd2 = histadd1;

    //histogramms must have the same step start and end , otherwise error
    histadd1.add(histadd1, histadd2);


    Histogram hist(1, 10, 10);
    for (int i = 0; i < 1000; ++i) {
        hist.fill(d(gen));
    }

    hist.printHistogram();
    std::cout << hist.get_maximum()<<std::endl;
    std::cout << hist.get_maximum_bin()<<std::endl;
    std::cout << hist.get_integral()<<std::endl;
    std::cout << hist.get_std()<<std::endl;
}
