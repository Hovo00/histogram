#ifndef HISTOGRAM_H
#define HISTOGRAM_H
#include <vector>

class Histogram
{
public:
	Histogram(float, float, float);
	float get_bin_width();
	int get_bin(float x);
	int fill(float x);
	float get_integral();
	float get_mean();

	//return index of bin with maximum count
	int get_maximum_bin();

	//return index of bin with minumum count
	int get_minimum_bin();

	//return value of bin with max or min elem
	float get_minimum();
	float get_maximum();


	void printHistogram();
	int getBinContent(int);

	//histogramms must have the same step start and end , otherwise error
	Histogram add(const Histogram&, const Histogram&);

	Histogram& operator =(Histogram&);

	float get_std();
private:
	float start, end, step;
	int count;
	std::vector<float> hist;
};

#endif
