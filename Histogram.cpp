#include "Histogram.h"
#include <iostream>
#include <cmath>
#include <iomanip>
#include <stdexcept>

Histogram::Histogram(float start = 0, float end = 10, float count = 10)
{
	if (start >= end) {
		throw std::invalid_argument(" received argument end lower or equal then start ");
	}
	this->start = start;
	this->end = end;
	this->count = count;
	this->step = (end - start) / count;
	for (int i = 0; i < count; ++i) {
		this->hist.push_back(0);
	}
}

float Histogram::get_bin_width(){
	return this->step;
}

int Histogram::get_bin(float x){

	if (x < start || x > end) return -1;
	return std::floor((x - start) / step);
}

int Histogram::fill(float x){
	if (x < start || x > end) return -1;
	this->hist[std::floor((x - start) / step)]++;

}

float Histogram::get_integral(){
	float integral = 0;
	for (auto p : hist) {
		integral += p;
	}
	return integral * step;
}

float Histogram::get_mean(){
	return (start + end) / 2;
}

//return index of bin with maximum count
int Histogram::get_maximum_bin(){
	int max = hist[0];
	int bin = 0;
	for (int i = 1; i < count; ++i) {
		if (hist[i] > max) {
			max = hist[i];
			bin = i;
		}
	}
	return bin;
}

//return index of bin with minumum count
int Histogram::get_minimum_bin() {
	int min = hist[0];
	int bin = 0;
	for (int i = 1; i < count; ++i) {
		if (hist[i] < min) {
			min = hist[i];
			bin = i;
		}
	}
	return bin;
}

//next 2 functions return value of bin with max or min elem

float Histogram::get_minimum() {

	return start +  get_minimum_bin() * step;
}
float Histogram::get_maximum() {

	return start + get_maximum_bin() * step;
}

void Histogram::printHistogram() {
	float st = 0;
	for (int i = 0; i < count; ++i) {
		std::cout << std::setw(4)
			<< start + st << " - " << start + st + step <<' ' << std::string(hist[i] / 2, '*') << '\n';
		st += step;
	}
}
int Histogram::getBinContent(int bin) {
	return hist[bin];
}


//histogramms must have the same step start and end , otherwise error
Histogram Histogram::add(const Histogram& h1, const Histogram& h2){

	if (h1.step != h2.step || h1.start != h2.start || h1.end != h2.end){
		throw std::invalid_argument("Histograms dont have the same step start and end");
	}
	Histogram histik = h1;
	int it = 0;
	for (auto p : histik.hist) {
		p += h2.hist[it++];
	}
	return histik;
}

Histogram& Histogram::operator=(Histogram& hiso){
	this->start = hiso.start;
	this->end = hiso.end;
	this->step = hiso.step;
	this->count = hiso.count;
	this->hist = hiso.hist;
	return *this;
}

float Histogram::get_std(){
	float temp = start;
	int it = 0;
	float Mean = 0;
	int freq = 0;
	while (temp <= this->end) {
		freq += getBinContent(it);
		Mean += (temp + step) / 2 * getBinContent(it);
		temp += step;
	}
	Mean /= freq;
	temp = start;
	it = 0;
	float StdDev = 0;
	while (temp <= this->end) {
		StdDev += (((temp + step) / 2) - Mean) * (((temp + step) / 2) - Mean) * getBinContent(it);
		temp += step;
	}
	return StdDev / freq;

}
